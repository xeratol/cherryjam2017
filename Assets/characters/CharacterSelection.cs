﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelection : MonoBehaviour {

    public GameObject selectionIndicator;
    private MoveIndicator moveIndicator;
    private ThrowRangeIndicator throwRangeIndicator;
    private CharacterCommandExecutor commandExecutor;

    private void Start()
    {
        Debug.Assert(selectionIndicator != null, "Selection Indicator not set", this);
        moveIndicator = GetComponentInChildren<MoveIndicator>();
        Debug.Assert(moveIndicator != null, "Move Indicator not set", this);
        throwRangeIndicator = GetComponentInChildren<ThrowRangeIndicator>();
        Debug.Assert(throwRangeIndicator != null, "Throw Range Indicator not set", this);
        commandExecutor = GetComponent<CharacterCommandExecutor>();
        Debug.Assert(commandExecutor != null, "command executor is missing", this);

        selectionIndicator.SetActive(false);
        moveIndicator.gameObject.SetActive(false);
        throwRangeIndicator.gameObject.SetActive(false);
    }

    private void OnMouseDown()
    {
        if (GameStateManager.instance.state == GameStateManager.GameState.Play) return;

        SelectionManager.instance.SetSelected(this);
    }

    public void Select()
    {
        selectionIndicator.SetActive(true);
    }

    public void Deselect()
    {
        selectionIndicator.SetActive(false);
    }

    public void BeginMove()
    {
        moveIndicator.gameObject.SetActive(true);
        moveIndicator.BeginSettingDestination();
    }

    public void Wait()
    {
        moveIndicator.gameObject.SetActive(false);
        throwRangeIndicator.Cancel();
        commandExecutor.Wait();
        Deselect();
    }

    public void BeginItemThrow(int i)
    {
        throwRangeIndicator.gameObject.SetActive(true);
        throwRangeIndicator.BeginSettingDestination(i);
    }
}
