﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEnable : MonoBehaviour {

    public int index = 2;

    void Start ()
    {
        var jellyCount = PlayerPrefs.GetInt("JELLY_COUNT", 0);
        if (jellyCount < index)
        {
            gameObject.SetActive(false);
        }
    }
}
