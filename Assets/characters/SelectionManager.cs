﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectionManager : MonoBehaviour {

    public static SelectionManager instance { get; private set; }
    public CharacterSelection lastSelected { get; private set; }
    private bool hasSelectedThisFrame = false;

    public RectTransform characterActionMenu;

    public RectTransform characterItemMenu;

    private void Awake()
    {
        Debug.Assert(instance == null, "No two Party Managers may exist", this);
        instance = this;

        Debug.Assert(characterActionMenu != null, "Character Action Menu not set", this);
        characterActionMenu.gameObject.SetActive(false);

        Debug.Assert(characterItemMenu != null, "Character Item Menu not set", this);
        characterItemMenu.gameObject.SetActive(false);
    }

    public void SetSelected(CharacterSelection c)
    {
        if (lastSelected == c) return;

        if (lastSelected != null)
        {
            lastSelected.Deselect();
        }

        c.Wait();
        characterItemMenu.gameObject.SetActive(false);

        var screenSpace = RectTransformUtility.WorldToScreenPoint(Camera.main, c.transform.position);
        characterActionMenu.anchoredPosition = screenSpace - new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);
        characterActionMenu.gameObject.SetActive(true);

        c.Select();
        lastSelected = c;
        hasSelectedThisFrame = true;
    }

    void LateUpdate()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;

        if (!hasSelectedThisFrame && Input.anyKeyDown && lastSelected != null)
        {
            lastSelected.Deselect();
            lastSelected = null;

            characterActionMenu.gameObject.SetActive(false);
            characterItemMenu.gameObject.SetActive(false);
        }

        hasSelectedThisFrame = false;
    }

    public void BeginMove()
    {
        lastSelected.BeginMove();
    }

    public void Wait()
    {
        lastSelected.Wait();
        lastSelected = null;
    }

    public void OpenItemMenu()
    {
        var screenSpace = RectTransformUtility.WorldToScreenPoint(Camera.main, lastSelected.transform.position);
        screenSpace.x = Mathf.Clamp(screenSpace.x, 0, Screen.width - characterItemMenu.sizeDelta.x);
        screenSpace.y = Mathf.Clamp(screenSpace.y, characterItemMenu.sizeDelta.y, Screen.height);

        var pos = screenSpace - new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);
        characterItemMenu.anchoredPosition = pos;
        characterItemMenu.gameObject.SetActive(true);
    }

    public void BeginItemUse(int i)
    {
        lastSelected.BeginItemThrow(i);
    }
}
