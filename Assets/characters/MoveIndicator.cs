﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveIndicator : MonoBehaviour {

    private Plane groundPlane;
    public Transform destinationIndicator;
    public CharacterCommandExecutor commandExecutor;

    private enum IndicatorState
    {
        Idle,
        SettingDestination,
        DestinationSet
    };
    private IndicatorState state = IndicatorState.Idle;

    private void Awake()
    {
        Debug.Assert(destinationIndicator != null, "destination indicator not set", this);
        destinationIndicator.SetParent(null);
        groundPlane = new Plane(Vector3.up, 0);

        Debug.Assert(commandExecutor != null, "command executor is missing", this);
    }

    public void BeginSettingDestination()
    {
        destinationIndicator.gameObject.SetActive(true);
        state = IndicatorState.SettingDestination;
    }

    private void OnDisable()
    {
        Cancel();
    }

    void Update()
    {
        switch (state)
        {
            case IndicatorState.SettingDestination:
                SettingDestination();
                break;
            default:
                break;
        }
    }

    public void UpdateGuide()
    {
        var relPos = destinationIndicator.position - transform.position;
        var distance = relPos.magnitude;
        var scale = transform.localScale;
        scale.z = distance;
        transform.localScale = scale;
    }

    void SettingDestination()
    { 
        var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        var t = 0f;
        if (groundPlane.Raycast(mouseRay, out t))
        {
            var pos = mouseRay.GetPoint(t);
            // TODO check for obstacles
            destinationIndicator.position = pos + Vector3.up * 0.01f;

            var relPos = pos - transform.position;

            var distance = relPos.magnitude;
            var scale = transform.localScale;
            scale.z = distance;
            transform.localScale = scale;

            var angle = Mathf.Atan2(relPos.x, relPos.z);
            transform.rotation = Quaternion.AngleAxis(angle * Mathf.Rad2Deg, Vector3.up);

            if (Input.GetMouseButtonDown(0))
            {
                commandExecutor.Move(pos);
                state = IndicatorState.DestinationSet;
            }
            else if (Input.anyKeyDown)
            {
                Cancel();
            }
        }
    }

    public void Cancel()
    {
        state = IndicatorState.Idle;
        gameObject.SetActive(false);

        if (destinationIndicator != null)
            destinationIndicator.gameObject.SetActive(false);
    }
}
