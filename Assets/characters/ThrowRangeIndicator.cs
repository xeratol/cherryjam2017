﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowRangeIndicator : MonoBehaviour {

    private Plane groundPlane;
    public Transform itemEffectRange;
    public float range = 20;
    public CharacterCommandExecutor commandExecutor;
    private int itemIndex;

    private enum IndicatorState
    {
        Idle,
        SettingDestination,
        DestinationSet
    };
    private IndicatorState state = IndicatorState.Idle;

    private void Awake()
    {
        Debug.Assert(itemEffectRange != null, "item effect range not set", this);
        groundPlane = new Plane(Vector3.up, 0);
        transform.localScale = new Vector3(range, range, range);
        itemEffectRange.gameObject.SetActive(false);

        Debug.Assert(commandExecutor != null, "command executor is missing", this);
    }

    public void BeginSettingDestination(int i)
    {
        itemIndex = i;
        itemEffectRange.gameObject.SetActive(true);
        state = IndicatorState.SettingDestination;
    }

    private void OnDisable()
    {
        state = IndicatorState.Idle;
    }

    void Update ()
    {
        switch (state)
        {
            case IndicatorState.SettingDestination:
                SettingDestination();
                break;
            default:
                break;
        }
    }

    void SettingDestination()
    {
        var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        var t = 0f;
        if (groundPlane.Raycast(mouseRay, out t))
        {
            var pos = mouseRay.GetPoint(t);
            var relPos = pos - transform.position;
            if (relPos.sqrMagnitude > range * range * 0.25f)
            {
                pos = transform.position + relPos.normalized * range * 0.5f;
                relPos = pos - transform.position;
            }

            itemEffectRange.position = pos + Vector3.up * 0.01f;

            if (Input.GetMouseButtonDown(0))
            {
                commandExecutor.UseItem(pos, itemIndex);
                gameObject.SetActive(false);
                state = IndicatorState.DestinationSet;
            }
            else if (Input.anyKeyDown)
            {
                Cancel();
            }
        }
    }

    public void Cancel()
    {
        state = IndicatorState.Idle;
        gameObject.SetActive(false);
        itemEffectRange.gameObject.SetActive(false);
    }
}
