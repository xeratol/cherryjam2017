﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCommandExecutor : MonoBehaviour {

    private enum Command
    {
        Wait,
        Move,
        UseItem,
    }
    private Command command = Command.Wait;
    private Vector3 destination;
    private Vector3 direction;
    private int item;

    public float moveDestinationThreshold = 0.2f;
    public float moveSpeed = 10;
    private MoveIndicator moveIndicator;

    private void Start()
    {
        moveIndicator = GetComponentInChildren<MoveIndicator>();
        Debug.Assert(moveIndicator != null, "Move Indicator not set", this);
    }

    void Update ()
    {
        if (GameStateManager.instance.state != GameStateManager.GameState.Play) return;

        switch (command)
        {
            case Command.Wait:
                break;
            case Command.Move:
                UpdateMove();
                break;
            case Command.UseItem:
                UpdateUseItem();
                Wait();
                break;
        }
    }

    void UpdateMove()
    {
        var distSq = (destination - transform.position).sqrMagnitude;
        if (distSq > moveDestinationThreshold * moveDestinationThreshold)
        {
            transform.position += Time.deltaTime * direction * moveSpeed;
            moveIndicator.UpdateGuide();
        }
        else
        {
            moveIndicator.Cancel();
            Wait();
        }
    }

    public void Move(Vector3 d)
    {
        command = Command.Move;
        destination = d;
        direction = (destination - transform.position).normalized;
    }

    void UpdateUseItem()
    {
        var itemInstance = PartyInventory.instance.ConsumeItem(item);
        if (itemInstance != null)
        {
            itemInstance.position = transform.position;
            itemInstance.rotation = Quaternion.identity;
            itemInstance.GetComponent<ItemBehavior>().SetDestination(destination);
        }
    }

    public void UseItem(Vector3 d, int i)
    {
        command = Command.UseItem;
        destination = d;
        item = i;
        direction = (destination - transform.position).normalized;
    }

    public void Wait()
    {
        command = Command.Wait;
    }

    private void OnCollisionEnter()
    {
        moveIndicator.Cancel();
        Wait();
    }
}
