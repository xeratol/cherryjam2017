﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestBehavior : MonoBehaviour {

    public Transform acquireEffect;
    public int itemIndex = -1;
    public int quantity = -1;

    void OnCollisionEnter ()
    {
        var index = (itemIndex < 0) ? Random.Range(0, PartyInventory.instance.items.Length) : itemIndex;
        var amount = (quantity < 1) ? Random.Range(1, 2) : quantity;
        PartyInventory.instance.AcquireItem(index, amount);

        Instantiate(acquireEffect, transform.position, Quaternion.Euler(-90, 0, 0));

        Destroy(gameObject);
    }
}
