﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehavior : MonoBehaviour {

    private Vector3 destination;
    private Vector3 direction;
    public float speed = 30;
    public float distanceThreshold = 0.2f;
    public Transform effect;

    public void SetDestination(Vector3 d)
    {
        destination = d;
        direction = (d - transform.position).normalized;
    }
    
    void Update ()
    {
        if (GameStateManager.instance.state != GameStateManager.GameState.Play) return;

        if ((destination - transform.position).sqrMagnitude > distanceThreshold * distanceThreshold)
        {
            transform.position += direction * speed * Time.deltaTime;
        }
        else
        {
            Instantiate(effect, transform.position, Quaternion.Euler(-90, 0, 0));
            Destroy(gameObject);
        }
    }
}
