﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item03Effect : MonoBehaviour {

    ParticleSystem particles;
    public LayerMask layer;

    void Start ()
    {
        particles = GetComponent<ParticleSystem>();

        var hits = Physics.SphereCastAll(transform.position, 4, Vector3.up,
            Mathf.Infinity, layer);
        foreach (var hit in hits)
        {
            var chaseBehavior = hit.transform.GetComponent<GhostChaseBehavior>();
            chaseBehavior.SetTarget(null);
        }
    }

    void Update ()
    {
        if (!particles.isPlaying)
        {
            Destroy(gameObject);
        }
    }
}
