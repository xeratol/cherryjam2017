﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyInventory : MonoBehaviour {

    public Transform[] items;

    public static PartyInventory instance { get; private set; }

    void Awake ()
    {
        Debug.Assert(instance == null, "only one Party Inventory may exist", this);
        instance = this;
    }
    
    public bool HasItem(int index)
    {
        return PlayerPrefs.GetInt("ITEM_" + index + "_COUNT", 0) > 0;
    }

    public Transform ConsumeItem(int index)
    {
        var count = PlayerPrefs.GetInt("ITEM_" + index + "_COUNT", 0);
        if (count < 1) return null;
        PlayerPrefs.SetInt("ITEM_" + index + "_COUNT", count - 1);
        return Instantiate(items[index]);
    }

    public void AcquireItem(int index, int amount)
    {
        var count = PlayerPrefs.GetInt("ITEM_" + index + "_COUNT", 0);
        count += amount;
        PlayerPrefs.SetInt("ITEM_" + index + "_COUNT", count);
    }
}
