﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item01Effect : MonoBehaviour {

    ParticleSystem particles;
    public LayerMask layer;

    void Start ()
    {
        var hits = Physics.SphereCastAll(transform.position, 4, Vector3.up,
            Mathf.Infinity, layer);
        foreach (var hit in hits)
        {
            hit.transform.gameObject.SetActive(false);
        }

        particles = GetComponent<ParticleSystem>();
    }
    
    void Update ()
    {
        if (!particles.isPlaying)
        {
            Destroy(gameObject);
        }
    }
}
