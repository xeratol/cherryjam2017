﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item02Effect : MonoBehaviour {

    ParticleSystem particles;
    List<Transform> affected;
    public float slowSpeed = 1;
    bool wasPaused = true;

    void Start ()
    {
        particles = GetComponent<ParticleSystem>();
        affected = new List<Transform>();
    }

    void Update ()
    {
        if (GameStateManager.instance.state != GameStateManager.GameState.Play)
        {
            particles.Pause();
            wasPaused = true;
            return;
        }
        if (wasPaused)
        {
            particles.Play();
            wasPaused = false;
        }

        if (!particles.isPlaying)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        affected.Add(other.transform);
        other.SendMessage("SetSpeed", slowSpeed);
    }

    private void OnTriggerExit(Collider other)
    {
        affected.Remove(other.transform);
        other.SendMessage("RestoreSpeed");
    }

    private void OnDestroy()
    {
        foreach (var a in affected)
        {
            if (a == null) continue;
            a.SendMessage("RestoreSpeed");
        }
    }
}
