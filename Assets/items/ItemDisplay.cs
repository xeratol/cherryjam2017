﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDisplay : MonoBehaviour {

    public int index;
    public Text quantity;

    void OnEnable ()
    {
        var count = PlayerPrefs.GetInt("ITEM_" + index + "_COUNT", 0);
        quantity.text = count.ToString();

        GetComponent<Button>().interactable = (count > 0);
    }
}
