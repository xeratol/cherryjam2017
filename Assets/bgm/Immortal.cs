﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Immortal : MonoBehaviour {

    private static Immortal instance;

    void Awake ()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
