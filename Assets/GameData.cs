﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour {

    void Start ()
    {
        PlayerPrefs.SetInt("JELLY_COUNT", 2);
        PlayerPrefs.SetInt("ITEM_0_COUNT", 0);
        PlayerPrefs.SetInt("ITEM_1_COUNT", 0);
        PlayerPrefs.SetInt("ITEM_2_COUNT", 0);
    }
}
