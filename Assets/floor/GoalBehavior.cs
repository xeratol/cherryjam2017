﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalBehavior : MonoBehaviour {

    int numJelliesInside = 0;

    private void OnTriggerEnter(Collider other)
    {
        numJelliesInside++;
        var numLivingJellies = PlayerPrefs.GetInt("JELLY_COUNT", 0);
        if (numLivingJellies == numJelliesInside)
        {
            GameStateManager.instance.SwitchToWin();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        numJelliesInside--;
    }
}
