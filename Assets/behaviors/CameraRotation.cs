﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

    public float sensitivity = 0.1f;
    Vector3 lastMousePos;
    const int RIGHT_MOUSE_BUTTON = 1;

    private void Update()
    {
        if (Input.GetMouseButtonDown(RIGHT_MOUSE_BUTTON))
        {
            lastMousePos = Input.mousePosition;
        }
        else if (Input.GetMouseButton(RIGHT_MOUSE_BUTTON))
        {
            var mousePos = Input.mousePosition;
            var deltaMousePos = mousePos - lastMousePos;
            lastMousePos = mousePos;

            transform.rotation *= Quaternion.AngleAxis(deltaMousePos.x * sensitivity, Vector3.up);
        }
    }
}
