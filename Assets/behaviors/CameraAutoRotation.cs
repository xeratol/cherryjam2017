﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAutoRotation : MonoBehaviour {

    public float speed = 1;

    void Update ()
    {
        transform.rotation *= Quaternion.AngleAxis(speed * Time.deltaTime, Vector3.up);
    }
}
