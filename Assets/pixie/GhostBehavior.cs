﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBehavior : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(collision.gameObject);

        var jellyCount = PlayerPrefs.GetInt("JELLY_COUNT", 0);
        PlayerPrefs.SetInt("JELLY_COUNT", jellyCount - 1);

        if (jellyCount - 1 < 1)
        {
            GameStateManager.instance.SwitchToLose();
        }
    }
}
