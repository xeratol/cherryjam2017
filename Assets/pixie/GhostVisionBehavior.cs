﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostVisionBehavior : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        transform.parent.SendMessage("OnEnteredVision", other.transform);
    }
}
