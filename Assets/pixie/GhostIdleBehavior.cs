﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostIdleBehavior : MonoBehaviour {

    GhostChaseBehavior chaseBehavior;

    private void Awake()
    {
        chaseBehavior = GetComponent<GhostChaseBehavior>();
        Debug.Assert(chaseBehavior != null, "chase behavior not set", this);
    }

    void OnEnteredVision(Transform transform)
    {
        chaseBehavior.SetTarget(transform);
    }
}
