﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostPatrolBehavior : MonoBehaviour {

    public Vector3 [] waypoints;
    public float distanceThreshold = 0.2f;
    private int currentIndex = 0;
    private Vector3 direction;
    public float speed = 2;
    private float origSpeed;
    bool isChasing = false;

    GhostChaseBehavior chaseBehavior;

    private void Awake()
    {
        chaseBehavior = GetComponent<GhostChaseBehavior>();
        Debug.Assert(chaseBehavior != null, "chase behavior not set", this);

        Debug.Assert(waypoints.Length > 1, "not enough waypoints set", this);
        direction = (waypoints[currentIndex] - transform.position).normalized;

        origSpeed = speed;
    }

    void Update ()
    {
        if (GameStateManager.instance.state != GameStateManager.GameState.Play) return;
        if (isChasing)
        {
            if (chaseBehavior.enabled)
            {
                return;
            }
            else
            {
                isChasing = false;
                direction = (waypoints[currentIndex] - transform.position).normalized;
            }
        }

        var distSq = (waypoints[currentIndex] - transform.position).sqrMagnitude;
        if (distSq < distanceThreshold)
        {
            currentIndex = (currentIndex + 1) % waypoints.Length;
            direction = (waypoints[currentIndex] - transform.position).normalized;
        }

        transform.position += direction * speed * Time.deltaTime;
    }

    void OnEnteredVision(Transform transform)
    {
        chaseBehavior.SetTarget(transform);
        isChasing = true;
    }

    private void OnDrawGizmosSelected()
    {
        if (waypoints == null) return;

        foreach (var waypoint in waypoints)
        {
            Gizmos.DrawSphere(waypoint, 0.5f);
        }
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public void RestoreSpeed()
    {
        speed = origSpeed;
    }
}
