﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostChaseBehavior : MonoBehaviour {

    Transform target;
    public float speed = 10;
    private float origSpeed;

    private void Awake()
    {
        origSpeed = speed;
    }

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
        enabled = true;
    }

    void Update ()
    {
        if (target == null)
        {
            enabled = false;
            return;
        }

        if (GameStateManager.instance.state != GameStateManager.GameState.Play) return;

        var direction = (target.position - transform.position).normalized;
        transform.position += direction * speed * Time.deltaTime;
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public void RestoreSpeed()
    {
        speed = origSpeed;
    }
}
