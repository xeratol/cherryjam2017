﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour {

    private CharacterSelection [] characterSelections;
    public GameObject[] objectsToHideOnEnd;
    public GameObject[] objectsToShowOnEnd;
    public GameObject[] objectsToShowOnLose;

    public enum GameState
    {
        Pause,
        Play,
        Win,
        Lose,
    }
    public GameState state { get; private set; }

    public static GameStateManager instance { get; private set; }

    private void Awake()
    {
        Debug.Assert(instance == null, "only one game state manager may exist", this);
        instance = this;
    }

    void Start ()
    {
        characterSelections = GameObject.FindObjectsOfType<CharacterSelection>();
        state = GameState.Pause;
    }
    
    public void SwitchToPlay()
    {
        foreach (var c in characterSelections)
        {
            if (c == null) continue;
            c.enabled = false;
        }
        state = GameState.Play;
    }

    public void SwitchToWin()
    {
        state = GameState.Win;
        foreach (var obj in objectsToHideOnEnd)
        {
            obj.SetActive(false);
        }
        foreach (var obj in objectsToShowOnEnd)
        {
            obj.SetActive(true);
        }
    }

    public void SwitchToLose()
    {
        state = GameState.Lose;
        foreach (var obj in objectsToHideOnEnd)
        {
            obj.SetActive(false);
        }
        foreach (var obj in objectsToShowOnLose)
        {
            obj.SetActive(true);
        }
    }

    public void SwitchToPause()
    {
        foreach (var c in characterSelections)
        {
            if (c == null) continue;
            c.enabled = true;
        }
        state = GameState.Pause;
    }
}
